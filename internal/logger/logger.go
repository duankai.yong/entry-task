package logger

import (
	"log"
	"os"
)

var level = 3

type CustomLogger struct {
	debugLogger *log.Logger
	infoLogger  *log.Logger
	errorLogger *log.Logger
	level       int
}

func NewLogger(filename string) (*CustomLogger, error) {
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return nil, err
	}

	logger := &CustomLogger{
		debugLogger: log.New(file, "DEBUG: ", log.Ldate|log.Ltime|log.Lshortfile),
		infoLogger:  log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile),
		errorLogger: log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile),
		level:       level,
	}

	return logger, nil
}

func (logger *CustomLogger) LogDebug(log string) {
	if logger.level >= 3 {
		logger.debugLogger.Println(log)
	}
}

func (logger *CustomLogger) LogInfo(log string) {
	if logger.level >= 2 {
		logger.infoLogger.Println(log)
	}
}

func (logger *CustomLogger) LogError(log string) {
	if logger.level >= 1 {
		logger.errorLogger.Println(log)
	}
}
