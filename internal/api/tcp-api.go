package api

import (
	"encoding/binary"
	"io"
	"net"
)

// codes
const Success string = "00"
const Failed string = "01"
const Database string = "21"
const Cache string = "22"
const Username string = "40"
const Password string = "41"
const Session string = "42"

const Login string = "login"
const Profile string = "profile"
const Edit string = "edit"

const prefixSize = 4

func Encode(str string) []byte {
	data := []byte(str)
	buf := make([]byte, prefixSize+len(data))
	binary.BigEndian.PutUint32(buf[:prefixSize], uint32(prefixSize+len(data)))
	copy(buf[prefixSize:], data[:])
	return buf
}

func Decode(conn net.Conn) (string, error) {
	prefix := make([]byte, prefixSize)
	_, err := io.ReadFull(conn, prefix)
	if err != nil {
		return Failed, err
	}
	dataLength := binary.BigEndian.Uint32(prefix[:])
	data := make([]byte, dataLength-prefixSize)
	_, err = io.ReadFull(conn, data)
	if err != nil {
		return Failed, err
	}
	return string(data), nil
}
