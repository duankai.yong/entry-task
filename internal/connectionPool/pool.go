package connectionPool

import "net"

type Pool interface {
	Get() (net.Conn, error)

	Put(net.Conn) error
}
