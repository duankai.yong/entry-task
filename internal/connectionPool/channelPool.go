package connectionPool

import (
	"errors"
	"fmt"
	"net"
	"sync"
	"time"
)

type ChannelPool struct {
	mu          sync.RWMutex
	conns       chan *idleConn
	factory     Factory
	curOpen     int
	maxOpen     int
	maxIdle     int
	connReqs    []*connReq
	idleTimeout time.Duration
	waitTimeout time.Duration
}

type Factory func() (net.Conn, error)

type idleConn struct {
	conn net.Conn
	t    time.Time
}

type connReq struct {
	connChan chan *idleConn
	errChan  chan error
}

func NewChanPool(maxOpen, maxIdle int, factory Factory, idleTimeout time.Duration) (*ChannelPool, error) {
	cp := &ChannelPool{
		conns:       make(chan *idleConn, maxIdle),
		factory:     factory,
		maxOpen:     maxOpen,
		maxIdle:     maxIdle,
		idleTimeout: idleTimeout,
	}

	for i := 0; i < maxIdle/2; i++ {
		conn, err := cp.factory()
		if err != nil {
			fmt.Println("unable to initialize idle connections")
			return nil, err
		}
		cp.conns <- &idleConn{conn: conn, t: time.Now()}
	}

	fmt.Println("connection pool", maxOpen, maxIdle, "created")

	return cp, nil
}

func (cp *ChannelPool) getConns() chan *idleConn {
	cp.mu.Lock()
	conns := cp.conns
	//fmt.Println(len(cp.conns))
	cp.mu.Unlock()
	return conns
}

func (cp *ChannelPool) Get() (net.Conn, error) {
	conns := cp.getConns()
	for {
		select {
		case wrappedConn := <-conns:
			//fmt.Println("case1")
			if checkTimeout(cp, wrappedConn) {
				continue
			}
			return wrappedConn.conn, nil
		default:
			cp.mu.Lock()
			if cp.curOpen < cp.maxOpen {
				fmt.Println("case2")
				cp.curOpen++
				cp.mu.Unlock()
				conn, err := cp.factory()
				if err != nil {
					cp.mu.Lock()
					cp.curOpen--
					cp.mu.Unlock()
					return nil, err
				}
				return conn, nil
			} else {
				fmt.Println("case3")
				req := &connReq{
					connChan: make(chan *idleConn, 1),
					errChan:  make(chan error, 1),
				}
				cp.connReqs = append(cp.connReqs, req)
				cp.mu.Unlock()
				select {
				case wrappedConn := <-req.connChan:
					fmt.Println("connection received")
					if checkTimeout(cp, wrappedConn) {
						continue
					}
					return wrappedConn.conn, nil
				case err := <-req.errChan:
					return nil, err
				}
			}
		}
	}
}

func (cp *ChannelPool) Put(conn net.Conn) error {
	if conn == nil {
		return errors.New("nil connection")
	}

	cp.mu.Lock()
	defer cp.mu.Unlock()

	if crs := len(cp.connReqs); crs > 0 {
		//fmt.Println("transferring connection", crs)
		req := cp.connReqs[0]
		cp.connReqs = cp.connReqs[1:]
		req.connChan <- &idleConn{conn: conn, t: time.Now()}
		return nil
	} else {
		select {
		case cp.conns <- &idleConn{conn: conn, t: time.Now()}:
			fmt.Println("returned to pool")
			return nil
		default:
			fmt.Println("closing connection")
			return conn.Close()
		}
	}
}

func checkTimeout(cp *ChannelPool, wrappedConn *idleConn) bool {
	if timeout := cp.idleTimeout; timeout > 0 {
		if wrappedConn.t.Add(timeout).Before(time.Now()) {
			fmt.Println("connection timed out; closing connection")
			cp.mu.Lock()
			cp.curOpen--
			cp.mu.Unlock()
			wrappedConn.conn.Close()
			return true
		}
	}
	return false
}
