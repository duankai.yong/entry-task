package connectionPool

import (
	"errors"
	"fmt"
	"net"
	"sync"
	"time"
)

type TcpConnPool struct {
	mu        sync.RWMutex
	idleConns chan net.Conn
	factory   Factory
	numOpen   int
	maxOpen   int
	maxIdle   int
	requests  chan *ConnReq
}

type ConnReq struct {
	connChan chan net.Conn
	errChan  chan error
}

func NewTcpConnPool(maxOpen, maxIdle int, factory Factory, idleTime time.Duration) (*TcpConnPool, error) {
	cp := &TcpConnPool{
		idleConns: make(chan net.Conn, maxIdle),
		factory:   factory,
		numOpen:   0,
		maxOpen:   maxOpen,
		maxIdle:   maxIdle,
		requests:  make(chan *ConnReq),
	}

	fmt.Println("connection pool", maxOpen, maxIdle, "created")

	go cp.handleConnReq()

	return cp, nil
}

func (cp *TcpConnPool) Put(conn net.Conn) error {
	fmt.Println("put connection called")

	if conn == nil {
		return errors.New("nil connection")
	}

	cp.mu.RLock()
	defer cp.mu.RUnlock()

	select {
	case cp.idleConns <- conn:
		cp.numOpen--
		return nil
	default:
		cp.numOpen--
		return conn.Close()
	}
}

func (cp *TcpConnPool) Get() (net.Conn, error) {
	fmt.Println("get connection called")

	cp.mu.RLock()

	select {
	// get an idle connection
	case conn := <-cp.idleConns:
		fmt.Println("case1", cp.numOpen)
		cp.numOpen++
		cp.mu.RUnlock()
		return conn, nil
	default:
		switch {
		// open a new connection
		case cp.numOpen < cp.maxOpen:
			fmt.Println("case2", cp.numOpen)
			conn, err := cp.factory()
			if err != nil {
				return nil, err
			}
			cp.numOpen++
			cp.mu.RUnlock()
			return conn, err
			//submit a request for a channel
		default:
			fmt.Println("case3", cp.numOpen)
			req := &ConnReq{
				connChan: make(chan net.Conn, 1),
				errChan:  make(chan error, 1),
			}
			cp.mu.RUnlock()
			cp.requests <- req
			select {
			case conn := <-req.connChan:
				return conn, nil
			case err := <-req.errChan:
				return nil, err
			}
		}
	}
}

func (cp *TcpConnPool) handleConnReq() {
	for req := range cp.requests {
		var (
			requestDone = false
			hasTimeout  = false
			timeoutChan = time.After(3 * time.Second)
		)
		for {
			if requestDone || hasTimeout {
				break
			}
			select {
			case <-timeoutChan:
				hasTimeout = true
				req.errChan <- errors.New("connection request timeout")
			default:
				cp.mu.RLock()

				select {
				// get an idle connection
				case conn := <-cp.idleConns:
					cp.numOpen++
					cp.mu.RUnlock()
					req.connChan <- conn
					requestDone = true
				default:
					switch {
					// open a new connection
					case cp.numOpen < cp.maxOpen:
						conn, err := cp.factory()
						if err != nil {
							continue
						}
						cp.numOpen++
						cp.mu.RUnlock()
						req.connChan <- conn
						requestDone = true
					default:
						cp.mu.RUnlock()
					}
				}
			}
		}
	}
}
