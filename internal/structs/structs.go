package structs

type LoginDetails struct {
	Username string
	Password string
}

type Session struct {
	Session string
}

type User struct {
	UserID   int64
	Username string //maximum length 255
	Password string //maximum length 72
	Nickname string //maximum length 255
	Picture  string //maximum length 255
}
