module entry-task

go 1.18

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20220511200225-c6db032c6c88
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/go-redis/cache/v8 v8.4.3 // indirect
	github.com/go-redis/redis/v8 v8.11.5 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/vmihailenco/go-tinylfu v0.2.2 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.4 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/exp v0.0.0-20210916165020-5cb4fee858ee // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
