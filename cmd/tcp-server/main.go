package main

import (
	"context"
	"database/sql"
	"entry-task/internal/api"
	logger2 "entry-task/internal/logger"
	"fmt"
	"github.com/go-redis/redis/v8"
	_ "github.com/go-sql-driver/mysql"
	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"net"
	"strings"
	"time"
)

var db *sql.DB
var err error
var logger *logger2.CustomLogger

var rdb *redis.Client
var ctx = context.Background()
var useSession = true
var sessionLife = time.Hour

func main() {
	logger, err = logger2.NewLogger("../../logs/tcp-log" + time.Now().String() + ".txt")
	fmt.Println("logging started")

	db, err = sql.Open("mysql", "root:mysqlpass@/ums_db")
	if err != nil {
		logger.LogError(err.Error())
	}
	db.SetMaxIdleConns(32)
	db.SetMaxOpenConns(32)
	logger.LogInfo("database connected")
	fmt.Println("database connected")

	rdb = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	_, err = rdb.Ping(ctx).Result()
	if err != nil {
		logger.LogError(err.Error())
	}
	logger.LogInfo("redis connected")
	fmt.Println("redis connected")

	ln, err := net.Listen("tcp", ":9000")
	if err != nil {
		logger.LogError(err.Error())
	}
	logger.LogInfo("listening on port 9000")
	fmt.Println("listening on port 9000")
	defer ln.Close()

	for {
		conn, err := ln.Accept()
		if err != nil {
			logger.LogError(err.Error())
		}
		go handler(conn)
	}
}

func handler(conn net.Conn) {
	for {
		netData, err := api.Decode(conn)
		//netData, err := bufio.NewReader(conn).ReadString('\n')
		if err != nil {
			logger.LogError(err.Error())
			return
		}

		pkt := strings.Fields(string(netData))
		logger.LogDebug(string(netData))

		switch pkt[0] {
		case api.Login:
			err = loginHandler(conn, pkt)
			if err != nil {
				logger.LogError(err.Error())
			}
		case api.Profile:
			err = profileHandler(conn, pkt)
			if err != nil {
				logger.LogError(err.Error())
			}
		case api.Edit:
			err = editHandler(conn, pkt)
			if err != nil {
				logger.LogError(err.Error())
			}
		}
	}
}

func loginHandler(conn net.Conn, pkt []string) error {
	logger.LogDebug("login handler called")

	username := pkt[1]
	password := pkt[2]

	result, err := authenticate(username, password)
	if err != nil {
		conn.Write(api.Encode(result))
		return err
	}

	if useSession {
		//create session here
		sID := uuid.NewV4().String()

		conn.Write(api.Encode(api.Success + " " + sID + " Welcome " + result))

		err := rdb.Set(ctx, sID, username, 0).Err()
		if err != nil {
			return err
		}
	} else {
		conn.Write(api.Encode(api.Success + " Welcome " + result))
	}

	return nil
}

func profileHandler(conn net.Conn, pkt []string) error {
	logger.LogDebug("profile handler called")

	session := pkt[1]
	username := pkt[2]

	// handle session authentication here
	valid, err := validateSession(session, username, conn)
	if !valid {
		return err
	}

	nickname, picture, err := getProfile(username)
	if err != nil {
		conn.Write(api.Encode(api.Database))
		return err
	}

	conn.Write(api.Encode(api.Success + " " + username + " " + nickname + " " + picture))
	return nil
}

func editHandler(conn net.Conn, pkt []string) error {
	logger.LogDebug("edit handler called")

	session := pkt[1]
	username := pkt[2]

	// handle session authentication here
	valid, err := validateSession(session, username, conn)
	if !valid {
		return err
	}

	nickname := pkt[3]
	picture := pkt[4]

	err = updateProfile(username, nickname, picture)
	if err != nil {
		conn.Write(api.Encode(api.Database))
		return err
	}

	conn.Write(api.Encode(api.Success))
	return nil
}

// database api functions

func authenticate(username string, password string) (string, error) {
	var dbPassword string

	dbPassword, err := rdb.Get(ctx, username).Result()
	if err != nil {
		err = db.QueryRow("SELECT user_password FROM user_tab WHERE user_username=?", username).Scan(&dbPassword)
		if err != nil {
			return api.Username, err
		}
		err = rdb.Set(ctx, username, dbPassword, sessionLife).Err()
		if err != nil {
			logger.LogError("unable to write session to cache: " + err.Error())
		}
	}

	err = bcrypt.CompareHashAndPassword([]byte(dbPassword), []byte(password))
	if err != nil {
		return api.Password, err
	}

	return username, nil
}

func getProfile(username string) (string, string, error) {
	var dbNickname, dbPicture string

	err := db.QueryRow("SELECT user_nickname, user_picture FROM user_tab WHERE user_username=?", username).Scan(&dbNickname, &dbPicture)
	if err != nil {
		return "", "", err
	}

	return dbNickname, dbPicture, nil
}

func updateProfile(username string, nickname string, picture string) error {
	_, err := db.Exec("UPDATE user_tab SET user_nickname = ?, user_picture = ? WHERE user_username = ?", nickname, picture, username)
	if err != nil {
		return err
	}

	return nil
}

func validateSession(session, username string, conn net.Conn) (bool, error) {
	if useSession {
		wanted, err := rdb.Get(ctx, session).Result()
		if err != nil {
			conn.Write(api.Encode(api.Cache))
			return false, err
		}
		if username != wanted {
			conn.Write(api.Encode(api.Session))
			logger.LogInfo(username + ":" + session + "; session does not match")
			return false, nil
		}
	}
	return true, nil
}
