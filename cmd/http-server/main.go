package main

import (
	"entry-task/internal/api"
	"entry-task/internal/connectionPool"
	logger2 "entry-task/internal/logger"
	"entry-task/internal/structs"
	"fmt"
	"html/template"
	"net"
	"net/http"
	"strings"
	"time"
)

var logger *logger2.CustomLogger
var err error

func main() {
	logger, err = logger2.NewLogger("../../logs/http-log" + time.Now().String() + ".txt")
	fmt.Println("logging started")

	factory := func() (net.Conn, error) { return net.Dial("tcp", "localhost:9000") }
	cp, err := connectionPool.NewChanPool(200, 200, factory, 15*time.Second)
	if err != nil {
		logger.LogError("unable to initialise connection pool:" + err.Error())
		panic(err)
	}

	http.HandleFunc("/login", login(cp))
	http.HandleFunc("/profile", profile(cp))
	http.HandleFunc("/", login(cp))
	http.ListenAndServe(":8080", nil)
}

func getConn(cp connectionPool.Pool) (net.Conn, error) {
	return cp.Get()
}

func putConn(cp connectionPool.Pool, conn net.Conn) error {
	return cp.Put(conn)
}

func login(cp connectionPool.Pool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logger.LogDebug("login route called")

		if r.Method != "POST" {
			logger.LogDebug("login page requested")
			http.ServeFile(w, r, "../../templates/login.html")
			logger.LogDebug("login page served")
			return
		}

		logger.LogDebug("logging in!")

		op := api.Login
		rUsr := r.FormValue("username")
		rPwd := r.FormValue("password")
		if rUsr == "" || rPwd == "" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		conn, err := getConn(cp)
		if err != nil {
			logger.LogError("unable to get connection: " + err.Error())
			return
		}
		defer putConn(cp, conn)

		outgoing := op + " " + rUsr + " " + rPwd
		conn.Write(api.Encode(outgoing))

		message, _ := api.Decode(conn)
		fmt.Println("->: " + message)
		logger.LogInfo("->: " + message)

		temp := strings.Fields(message)
		fmt.Println(temp)
		switch temp[0] {
		case api.Success:
			fmt.Println("success")
			// set a cookie in the browser
			cookie, err := r.Cookie("session")
			if err != nil {
				sID := temp[1]
				cookie = &http.Cookie{
					Name:  "session",
					Value: sID,
				}
			}
			http.SetCookie(w, cookie)
			fmt.Println("cookie set")

			logger.LogDebug("successful login; redirecting to profile page")
			http.Redirect(w, r, "/profile?username="+rUsr, http.StatusFound)
		default:
			logger.LogDebug("error code " + temp[0] + ": unsuccessful login")
			http.ServeFile(w, r, "../../templates/login.html")
		}
		return
	}
}

func profile(cp connectionPool.Pool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logger.LogDebug("profile route called")

		cookie, err := r.Cookie("session")
		if err != nil {
			// no session in browser
			logger.LogDebug("session not found, redirecting to login page")
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}

		rSess := cookie.Value
		//username via redirect
		rUsr := r.URL.Query().Get("username")
		//username via form-value
		if rUsr == "" {
			rUsr = r.FormValue("username")
		}
		if rSess == "" || rUsr == "" {
			w.WriteHeader(http.StatusBadRequest)
			logger.LogInfo("session or username invalid: " + rSess + ";" + rUsr)
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}

		conn, err := getConn(cp)
		if err != nil {
			logger.LogError("unable to get connection: " + err.Error())
			return
		}
		defer putConn(cp, conn)

		if r.Method != "POST" {
			//"GET"
			logger.LogDebug("getting profile!")

			op := api.Profile

			outgoing := op + " " + rSess + " " + rUsr
			conn.Write(api.Encode(outgoing))

			message, _ := api.Decode(conn)
			fmt.Println("->: " + message)
			logger.LogInfo("->: " + message)

			temp := strings.Fields(message)
			switch temp[0] {
			case api.Success:
				user := structs.User{Username: temp[1], Nickname: temp[2], Picture: temp[3]}
				renderTemplate(w, "profile.html", user)
				fmt.Println(user.Username + " profile retrieved")
				logger.LogInfo(user.Username + " profile retrieved")
			default:
				logger.LogDebug("error code " + temp[0] + ": unsuccessful profile edit")
				w.Write([]byte("error finding profile"))
			}
			return
		}

		logger.LogDebug("editing profile")

		op := api.Edit

		rNickname := r.FormValue("nickname")
		rPicture := r.FormValue("picture")

		outgoing := op + " " + rSess + " " + rUsr + " " + rNickname + " " + rPicture
		conn.Write(api.Encode(outgoing))

		message, _ := api.Decode(conn)
		fmt.Println("->: " + message)
		logger.LogInfo("->: " + message)

		temp := strings.Fields(message)
		switch temp[0] {
		case api.Success:
			w.Write([]byte("profile edited!"))
		default:
			logger.LogDebug("error code " + temp[0] + ": unsuccessful profile edit")
			w.Write([]byte("error editing profile"))
		}
		return
	}
}

var templates = template.Must(template.ParseFiles("../../templates/profile.html"))

func renderTemplate(w http.ResponseWriter, tmpl string, user structs.User) {
	err := templates.ExecuteTemplate(w, tmpl, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
