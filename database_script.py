import pymysql.cursors

conn = pymysql.connect(
    host='127.0.0.1',
    user='root',
    password='mysqlpass',
    database='ums_db',
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor
)

cursor = conn.cursor()

drop = """DROP TABLE IF EXISTS user_tab;"""
create = """CREATE TABLE user_tab (
  user_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  user_username VARCHAR(255) NOT NULL,
  user_password VARCHAR(255) NOT NULL DEFAULT 'password',
  user_nickname VARCHAR(255) DEFAULT 'nickname',
  user_picture VARCHAR(255) DEFAULT NULL,
  user_created DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (user_id),
  UNIQUE KEY user_username (user_username)
);"""

sql = """INSERT INTO user_tab (user_username, user_password, user_nickname, user_picture)
VALUES
("user_{}", "$2a$04$mS.q9QrmuLikYqdA6HkJ8uvgH4SSBkeaK5TxH5x4NtbyyoBBS78DK", "nickname", "https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,f_auto,q_auto:eco,dpr_1/itxiuybmyrbkkhrykvzi");
"""

cursor.execute(drop)
cursor.execute(create)

for i in range(1, 10000000):
    try:
        tmp_sql = sql.format(i)
        cursor.execute(tmp_sql)
        conn.commit()
    except Exception as e:
        print(e)

conn.commit()
conn.close()